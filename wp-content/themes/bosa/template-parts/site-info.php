<?php
/**
 * Template part for displaying site info
 *
 * @package Bosa
 */

?>

<div class="site-info">
	<?php echo wp_kses_post( html_entity_decode( esc_html__( 'Copyright &copy; ' , 'Ecoair Ventilations' ) ) );
		echo esc_html( date('Y') );
	?>
</div><!-- .site-info -->