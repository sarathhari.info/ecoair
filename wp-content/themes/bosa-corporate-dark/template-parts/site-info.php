<?php
/**
 * Template part for displaying site info
 *
 * @package Bosa Corporate Dark
 */

?>

<div class="site-info">
	<?php echo wp_kses_post( html_entity_decode( esc_html__( 'Copyright &copy; ' ) ) );
		echo esc_html( date( 'Y' ) );
	?>
	<a href="<?php echo esc_url( __( '/', 'Ecoair Ventilations' ) ); ?>">
		<?php
			printf( esc_html__( 'Ecoair Ventilations', 'Ecoair Ventilations' ) );
		?>
	</a>
</div><!-- .site-info -->