<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'i3300977_wp4' );

/** MySQL database username */
define( 'DB_USER', 'i3300977_wp4' );

/** MySQL database password */
define( 'DB_PASSWORD', 'B.HzU8Y5YVUJSteMXrB95' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'EL63WhbxNBY5l2dx2KIyFw0kvWBJkWhEEjJ9FfbPMnt1CxPlUTEB7S76UOsSA8UE');
define('SECURE_AUTH_KEY',  'peTbKDI9IGMi2teSrLqVPs1950BZATNuhoJV81wsKScKmoUZaelmM30B3qqXcMF6');
define('LOGGED_IN_KEY',    'WSR9TDfohiGXNKy8r8Rb9LDbjlQ7gBp46JKnPdLHEkJMjxsoULN1xyCPd0fbCXJV');
define('NONCE_KEY',        '9gUWizdMK4xOTqPdZ60WYrmxGVWciwt2OWgjVESypjshcXE4aBOse02BvqjaBdZe');
define('AUTH_SALT',        'boeFvS1J3yMeujGaapeDiM7N5flCyuRUgTKeityMpOETR8nRCgj7oj621Q2HNoMP');
define('SECURE_AUTH_SALT', 'BOdRjvIzCmANJbSdIsdKLf4FJJDdmsytoY3lx2p6DIV6cE97zXv2RcjCF0vbkL4w');
define('LOGGED_IN_SALT',   '4YgLWfEikv25IKOrRg4cjRA5Ntf6G8Fd2SdRg3T1Bxkz7BobI191SbUICuWyb5rE');
define('NONCE_SALT',       'gN3mf2QErWwPGgh1FXSPu4dq6xZDVQyONDXi44TLBvs9tQbRPdOvezWEfg4isXUr');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');
define('FS_CHMOD_DIR',0755);
define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed externally by Installatron.
 * If you remove this define() to re-enable WordPress's automatic background updating
 * then it's advised to disable auto-updating in Installatron.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
